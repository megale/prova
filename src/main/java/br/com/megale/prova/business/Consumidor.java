package br.com.megale.prova.business;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class Consumidor implements Runnable {

	private LinkedBlockingQueue<Integer> p;

	private LinkedBlockingQueue<Integer> f;

	public Consumidor(LinkedBlockingQueue<Integer> p, LinkedBlockingQueue<Integer> f) {
		this.p = p;
		this.f = f;
	}
	
	/**
	 * Iniciar execução da thread
	 */
	@Override
	public void run() {

		try {
			calcularResultado();
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * verificar números que repetem nas duas filas
	 * @throws InterruptedException exceção
	 */
	public void calcularResultado() throws InterruptedException {

		List<Integer> listaP = new ArrayList<Integer>();
		List<Integer> listaF = new ArrayList<Integer>();

		Integer numberP = new Integer(0);
		Integer numberF = new Integer(0);

		while (true) {
			numberP = verifyPrime(numberP, listaP, listaF);
			numberF = verifyFibonacci(numberF, listaP, listaF);
			if (isInTheEnd(numberP, numberF))
				break;
		}

		System.out.println("Sucesso!");

	}

	private Integer verifyFibonacci(Integer numberF, List<Integer> listaP, List<Integer> listaF) {

		if (!f.isEmpty()) {
			numberF = f.remove();
			if (!numberF.equals(-1)) {
				listaF.add(numberF);
				if (listaP.contains(numberF))
					System.out.println(numberF);
			}
		}

		return numberF;

	}

	private Integer verifyPrime(Integer numberP, List<Integer> listaP, List<Integer> listaF) {

		if (!p.isEmpty()) {
			numberP = p.remove();
			if (!numberP.equals(-1)) {
				listaP.add(numberP);
				if (listaF.contains(numberP))
					System.out.println(numberP);
			}
		}

		return numberP;

	}

	private boolean isInTheEnd(Integer numberP, Integer numberF) {
		return numberP.equals(-1) && numberF.equals(-1);
	}
}
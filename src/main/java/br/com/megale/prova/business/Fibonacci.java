package br.com.megale.prova.business;

import java.util.concurrent.LinkedBlockingQueue;

public class Fibonacci implements Runnable {

	private LinkedBlockingQueue<Integer> f;

	public Fibonacci(LinkedBlockingQueue<Integer> f) {
		this.f = f;
	}

	/**
	 * Início da thread
	 */
	@Override
	public void run() {

		try {
			calcFibonacci();
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Calcular némeros da sequencia de fibonacci
	 * 
	 * @throws InterruptedException exceção
	 */
	private void calcFibonacci() throws InterruptedException {

		for (Integer number = 0, b = 1, i = 0; number < 1000000; b += number, number = b - number, i++)
			f.add(number);
		f.add(-1);

	}
}
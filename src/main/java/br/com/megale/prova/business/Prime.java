package br.com.megale.prova.business;

import java.util.concurrent.LinkedBlockingQueue;

public class Prime implements Runnable {

	private LinkedBlockingQueue<Integer> p;

	public Prime(LinkedBlockingQueue<Integer> p) {
		this.p = p;
	}

	/**
	 * Início da thread
	 */
	@Override
	public void run() {

		try {
			calcPrime();
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Calcular os números primos
	 * 
	 * @throws InterruptedException exceção
	 */
	private void calcPrime() throws InterruptedException {

		for (int number = 2; number <= 1000000; number++)
			if (isPrime(number))
				p.add(number);
		p.add(-1);

	}

	/**
	 * Verificar se o número é um número primo
	 * 
	 * @param numero int - Número a ser verificado
	 * @return boolean
	 */
	private boolean isPrime(int numero) {

		for (int i = 2; i < numero; i++) {
			if (numero % i == 0)
				return false;
		}

		return true;

	}
}
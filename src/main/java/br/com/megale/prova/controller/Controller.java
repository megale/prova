package br.com.megale.prova.controller;

import java.util.concurrent.LinkedBlockingQueue;

import br.com.megale.prova.business.Consumidor;
import br.com.megale.prova.business.Fibonacci;
import br.com.megale.prova.business.Prime;

public class Controller {

	// Fila de números primos
	LinkedBlockingQueue<Integer> p;

	// Fila de números fibonacci
	LinkedBlockingQueue<Integer> f;

	public Controller() {
		p = new LinkedBlockingQueue<>();
		f = new LinkedBlockingQueue<>();
	}

	/**
	 * Iniciar execução do aplicativo
	 */
	private void start() {

		// Criação da thread p
		Prime prime = new Prime(p);
		Thread threadP = new Thread(prime, "Prime");

		// Criação da thread f
		Fibonacci fibonacci = new Fibonacci(f);
		Thread threadF = new Thread(fibonacci, "Fibonacci");

		// Criação da thread Consumidor
		Consumidor consumidor = new Consumidor(p, f);
		Thread threadC = new Thread(consumidor, "Consumer");

		// Iniciar threads
		threadP.start();
		threadF.start();
		threadC.start();

	}

	public static void main(String[] args) {
		new Controller().start();
	}
}